@extends("layouts.app")

@section("content")

<form>
    <div class="form-group row">
        <label for="txtWriter" class="col-sm-2 col-form-label">Writer</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="txtWriter" name="txtWriter" placeholder="Writer">
        </div>
    </div>

    <div class="form-group row">
        <label for="txtSubject" class="col-sm-2 col-form-label">Subject</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="txtSubject" name="txtSubject" placeholder="Subject">
        </div>
    </div>

    <div class="form-group row">
        <label for="txtBody" class="col-sm-2 col-form-label">Body</label>
        <div class="col-sm-10">
            <textarea class="form-control" name="txtBody" id="txtBody" placeholder="Body"></textarea>
        </div>
    </div>

    <div class="form-group row">
        <label for="txtBody" class="col-sm-2 col-form-label">Image</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="file" id="file">
        </div>
    </div>
    
    <div class="form-group row">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="/posts/" class="btn btn-warning">Cancel</a>
        </div>
    </div>
</form>

@endsection("content")
@extends("layouts.app")

@section("content")
    <table class="table table-hover">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Post Number</th>
                <th scope="col">Subject</th>
                <th scope="col">Writer</th>
                <th scope="col">Created on</th>
                <th scope="col">View</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>2018-ABC-0001</td>
                <td>Subject 1</td>
                <td>Juan</td>
                <td>March 1, 2018 08:00 AM</td>
                <td>
                    <a href="/posts/1" class="btn btn-primary">View</a>
                </td>
            </tr>

            <tr>
                <th scope="row">2</th>
                <td>2018-DEF-0002</td>
                <td>Subject 2</td>
                <td>Joseph</td>
                <td>March 3, 2018 08:00 AM</td>
                <td>
                    <a href="/posts/1" class="btn btn-primary">View</a>
                </td>
            </tr>

            <tr>
                <th scope="row">3</th>
                <td>2018-GHI-0003</td>
                <td>Subject 3</td>
                <td>Juan</td>
                <td>March 1, 2018 08:00 AM</td>
                <td>
                    <a href="/posts/1" class="btn btn-primary">View</a>
                </td>
            </tr>
        </tbody>
    </table>
@endsection("content")
@extends("layouts.app")

@section("content")
    <form>
        <div class="form-group row">
            <label for="txtWriter" class="col-sm-2 col-form-label">Writer</label>
            <div class="col-sm-10">
                <input type="email" class="form-control-plaintext" id="txtWriter" name="txtWriter" placeholder="Writer" value="Juan dela Cruz" readonly>
            </div>
        </div>
    
        <div class="form-group row">
            <label for="txtSubject" class="col-sm-2 col-form-label">Subject</label>
            <div class="col-sm-10">
                <input type="email" class="form-control-plaintext" id="txtSubject" name="txtSubject" placeholder="Subject" value="Subject 1" readonly>
            </div>
        </div>
    
        <div class="form-group row">
            <label for="txtBody" class="col-sm-2 col-form-label">Body</label>
            <div class="col-sm-10">
                <textarea class="form-control-plaintext" name="txtBody" id="txtBody" placeholder="Body" rows="30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in interdum nisi, a sodales nibh. Donec ornare facilisis dui, vitae pharetra ante venenatis quis. Cras non suscipit est. Duis rutrum vulputate commodo. Morbi at metus facilisis, dictum felis ut, tempus metus. Aenean justo enim, euismod porta dolor non, maximus fringilla augue. Pellentesque vehicula tortor auctor lacus dapibus pharetra.

                        Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent vehicula dui convallis vulputate lobortis. Sed aliquet ligula eu tristique consequat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec lorem nibh, finibus ac faucibus ut, condimentum non erat. Sed sodales tempus scelerisque. Nullam nec elit vel turpis vehicula pulvinar eu vel nisl. Aenean nec nulla vel felis lacinia tincidunt. Integer posuere nisi sit amet eros sagittis, vel dignissim massa volutpat. Sed viverra ornare arcu, vitae venenatis est convallis ut. Nulla aliquet bibendum congue. Quisque lacus sapien, tristique non volutpat a, fermentum non nisi. Duis vestibulum aliquet euismod.
                        
                        Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce pulvinar posuere consectetur. Suspendisse ac orci nec mauris posuere porttitor. Curabitur neque sem, imperdiet at accumsan id, egestas in turpis. Nam sed sodales ipsum. Donec dignissim varius lectus vitae sollicitudin. Duis gravida vitae sem et dignissim. Nulla leo mauris, congue non est nec, pharetra cursus ligula. Nam feugiat interdum nunc, sit amet ultricies purus mollis in. Proin rutrum ante quis semper hendrerit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed finibus risus sit amet ante lobortis vulputate.
                        
                        In commodo tincidunt quam, eu luctus ipsum vestibulum at. Praesent rhoncus eget neque eu lobortis. Suspendisse porta pulvinar massa, et congue arcu viverra ut. Donec at est aliquam leo pellentesque egestas quis ac nisi. Cras quis laoreet ante. Fusce pulvinar elementum facilisis. Maecenas tristique fermentum velit, sit amet bibendum nibh. Quisque est augue, vulputate at leo in, tempor volutpat urna. Aliquam erat volutpat. In vestibulum tincidunt arcu eget malesuada. Nam faucibus urna odio, vitae ullamcorper purus elementum quis. Morbi sed accumsan ipsum. Fusce facilisis at felis a pretium. Morbi in risus sodales, vehicula arcu sit amet, aliquam orci. In eget leo massa.
                        
                        Proin pretium lorem sed convallis accumsan. Proin placerat lacus et quam lacinia accumsan. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed suscipit diam velit, malesuada tempus libero dignissim non. Nunc augue justo, placerat eu gravida quis, aliquam eget tortor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur mattis elit interdum, lacinia felis nec, aliquam urna.</textarea>
            </div>
        </div>
    
        <div class="form-group row">
            <label for="txtBody" class="col-sm-2 col-form-label">Image</label>
            <div class="col-sm-10">
                <a href="#">Download</a>
            </div>
        </div>
        
        <div class="form-group row">
            <div class="col-sm-10">
                <a href="/posts/1/edit" class="btn btn-primary">Edit</a>
                <a href="/posts" class="btn btn-warning">Back to list</a>
            </div>
        </div>
    </form>
@endsection("content")